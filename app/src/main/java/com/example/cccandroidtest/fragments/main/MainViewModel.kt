package com.example.cccandroidtest.fragments.main

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.cccandroidtest.db.models.Estimate
import com.example.cccandroidtest.db.models.Person
import com.example.cccandroidtest.db.repository.EstimateRepository
import com.example.cccandroidtest.db.repository.PersonRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {


    fun addEstimationToDB(context: Context) {
        val estimation = Estimate(
            "c574b0b4-bdef-4b92-a8f0-608a86ccf5ab",
            "Placebo Secondary School",
            "32 Commissioners Road East",
            32,
            3,
            "2020-08-22 15:23:54",
            "85a57f85-a52d-4137-a0d1-62e61362f716",
            "85a57f85-a52d-4137-a0d1-62e61362f716",
            "85a57f85-a52d-4137-a0d1-62e61362f716"
        )
        EstimateRepository(context).addEstimation(estimation)
    }

    fun addPersonToDB(context: Context) {
        val person = Person(
            "85a57f85-a52d-4137-a0d1-62e61362f716",
            "Joseph",
            "Sham",
            "joseph.sham@fake.fake",
            "123-456-7890"
        )
        PersonRepository(context).addPerson(person)
    }

    fun getEstimation(context: Context): Estimate? {
        return EstimateRepository(context).getEstimation()
    }

    fun getPersonById(context: Context, id: String): Person? {
        return PersonRepository(context).getPerson(id)
    }
}