package com.example.cccandroidtest.fragments.main

import android.app.ProgressDialog
import android.util.Log
import android.view.View
import com.example.cccandroidtest.R
import com.example.cccandroidtest.base.BaseFragment
import com.example.cccandroidtest.databinding.FragmentMainBinding
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : BaseFragment<MainViewModel, FragmentMainBinding>() {

    override fun getViewModel(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun start() {
        addDataToDB()
        getPersonByEstimation()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_main
    }

    override fun setBinding(binding: FragmentMainBinding) {
    }

    private fun addDataToDB() {
        mViewModel.addEstimationToDB(mActivity)
        mViewModel.addPersonToDB(mActivity)

    }

    private fun getPersonByEstimation() {
        val estimate = mViewModel.getEstimation(mActivity)
        Log.d("ESTIMATE", estimate.toString())
        if (estimate?.created_by == estimate?.requested_by && estimate?.requested_by == estimate?.contact) {
            val person = mViewModel.getPersonById(mActivity, estimate!!.created_by)
            Log.d("PERSON", person.toString())
        }

    }
}