package com.example.cccandroidtest.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "person")
data class Person(
        @PrimaryKey(autoGenerate = false)
        var id :String,
        var first_name :String,
        var last_name :String,
        var email :String,
        var phone_number :String,
)

