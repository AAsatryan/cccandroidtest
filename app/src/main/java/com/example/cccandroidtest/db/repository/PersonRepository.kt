package com.example.cccandroidtest.db.repository

import android.content.Context
import com.example.cccandroidtest.db.dataBase.BaseDataBase
import com.example.cccandroidtest.db.models.Estimate
import com.example.cccandroidtest.db.models.Person
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class PersonRepository(context: Context) {

    private var db: BaseDataBase? = null

    init {
        db = BaseDataBase.getInstance(context)!!
    }

    fun addPerson(person: Person) {
        runBlocking(Dispatchers.IO) {
            db?.personDao()?.addPerson(person)
        }
    }

    fun getPerson(id: String): Person? {
        return runBlocking(Dispatchers.IO) {
            db?.personDao()?.getPerson(id)
        }
    }
}