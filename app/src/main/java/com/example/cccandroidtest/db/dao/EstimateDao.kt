package com.example.cccandroidtest.db.dao

import androidx.room.*
import com.example.cccandroidtest.db.models.Estimate

@Dao
interface EstimateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
   suspend fun addEstimation(estimate: Estimate)

    @Query("select  * From estimate LIMIT 1")
    suspend fun getEstimation(): Estimate
}