package com.example.cccandroidtest.db.dao

import androidx.room.*
import com.example.cccandroidtest.db.models.Estimate
import com.example.cccandroidtest.db.models.Person

@Dao
interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPerson(person: Person)

    @Query("select  * From person where id = :id")
    suspend fun getPerson(id: String): Person

}