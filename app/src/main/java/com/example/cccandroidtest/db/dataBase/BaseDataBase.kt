package com.example.cccandroidtest.db.dataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.cccandroidtest.db.dao.EstimateDao
import com.example.cccandroidtest.db.dao.PersonDao
import com.example.cccandroidtest.db.models.Estimate
import com.example.cccandroidtest.db.models.Person


@Database(entities = [Estimate::class, Person::class], version = 1)
abstract class BaseDataBase : RoomDatabase() {

    abstract fun estimateDao(): EstimateDao
    abstract fun personDao(): PersonDao

    companion object {
        private var INSTANCE: BaseDataBase? = null

        fun getInstance(context: Context): BaseDataBase? {
            if (INSTANCE == null) {
                synchronized(BaseDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        BaseDataBase::class.java, "userDataBase"
                    ).build()
                }
            }
            return INSTANCE
        }

    }


}