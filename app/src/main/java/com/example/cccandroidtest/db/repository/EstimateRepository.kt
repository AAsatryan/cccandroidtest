package com.example.cccandroidtest.db.repository

import android.content.Context
import com.example.cccandroidtest.db.dataBase.BaseDataBase
import com.example.cccandroidtest.db.models.Estimate
import kotlinx.coroutines.*

class EstimateRepository(context: Context) {

    private var db: BaseDataBase? = null

    init {
        db = BaseDataBase.getInstance(context)!!
    }

    fun addEstimation(estimate: Estimate) {
        runBlocking(Dispatchers.IO) {
            db?.estimateDao()?.addEstimation(estimate)
        }
    }

     fun getEstimation(): Estimate? {
        return runBlocking(Dispatchers.IO) {
            db?.estimateDao()?.getEstimation()
        }
    }
}