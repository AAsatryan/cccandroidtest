package com.example.cccandroidtest.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "estimate")
data class Estimate(

    @PrimaryKey(autoGenerate = false)
    var id: String,
    var company: String,
    var address: String,
    var number: Int,
    var revision_number: Int,
    var created_date: String,
    var created_by : String,
    var requested_by : String,
    var contact : String,
)
