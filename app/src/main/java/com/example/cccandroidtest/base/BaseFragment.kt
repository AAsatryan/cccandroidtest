package com.example.cccandroidtest.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.cccandroidtest.MainActivity

abstract class BaseFragment<MViewModel : ViewModel, Binding : ViewDataBinding> : Fragment() {

    lateinit var mActivity: MainActivity
    lateinit var mRootView: View
    lateinit var mViewModel: MViewModel
    lateinit var mBinding: Binding

    abstract fun getViewModel(): Class<MViewModel>

    abstract fun start()

    @LayoutRes
    abstract fun getLayoutId(): Int

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            val activity: MainActivity = context
            this.mActivity = activity
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(mActivity).get(getViewModel())
    }

    protected abstract fun setBinding(binding: Binding)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mRootView = inflater.inflate(getLayoutId(), container,false)
        mBinding = DataBindingUtil.inflate(inflater,getLayoutId(),container,false)

        setBinding(mBinding)
        mBinding.root.filterTouchesWhenObscured = true
        mRootView = mBinding.root


        return mRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start()

    }

}