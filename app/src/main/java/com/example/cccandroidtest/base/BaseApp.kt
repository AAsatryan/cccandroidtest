package com.example.cccandroidtest.base

import android.app.Application
import com.example.cccandroidtest.db.dataBase.BaseDataBase

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        BaseDataBase.getInstance(this)
    }
}